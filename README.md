# Convert your Mediawiki XML dump into a text file for ChatGPT (GPT)

## Overview

This tool is designed to extract content from MediaWiki XML dumps, transforming the data into a text format suitable for ingestion by ChaGPT. It's especially useful for those looking to utilize their MediaWiki content for AI training purposes, ensuring a seamless transition from wiki-based information to AI-readable format.

The Python script has been rigorously tested on a MediaWiki XML dump containing over 6,000,000 lines, ensuring its capability to process large datasets efficiently and reliably.   

## Prerequisites

- Python 3 installed on your system.
- An XML dump file from your MediaWiki instance.

## Features

- **Efficient Data Processing**: Optimized for large files, the script can handle XML dumps of significant size without compromising performance.
- **Simple CLI Usage**: Easy to use with a command-line interface, requiring minimal setup and arguments.
- **Custom Output**: Transforms MediaWiki titles and text content into a clean text format, making it GPT-friendly.

## Getting Started

### 1. Exporting Your MediaWiki Dump

Before using this script, you'll need to export an XML dump from your MediaWiki instance. This can typically be done with a [PHP maintenance script provided by MediaWiki](https://www.mediawiki.org/wiki/Manual:DumpBackup.php), such as `dumpBackup.php`. The command below can be executed in your MediaWiki's installation directory:

```bash 
php maintenance/dumpBackup.php --full --current > backup.xml
```

This command generates a full backup of your wiki, saving it as `backup.xml`.

### 2. Transforming Mediawiki xml dump to plain text

Once you have your XML dump, you can use the `mediawiki-to-chatgpt-converter.py` Python script to convert the dump into a text format. Here’s how to use the script:

    python mediawiki-to-chatgpt-converter.py <path_to_your_xml_dump> <output_file_path>

#### Command-Line Arguments:

- **`<path_to_your_xml_dump>`**: Path to your MediaWiki XML dump file.
- **`<output_file_path>`**: Path where the script will write the extracted text content.

#### Example:

    python mediawiki-to-chatgpt-converter.py backup.xml wiki_content.txt


## Output

The script generates an output file (`wiki_content.txt` in the example) containing the titles and text of your MediaWiki pages, formatted for easy ingestion by GPT models.

## Note

This script is optimized for efficiency, allowing it to process large XML dumps reliably. It's been tested with substantial data sizes, ensuring its suitability for various project scales.

## GPT instructions

Answer questions based on the wiki content provided as input.

Maintain a professional and informative tone consistent with the style and accuracy of the wiki content.
Avoid providing personal opinions or unverified information not present in the wiki.
Use wiki data to enrich answers by rephrasing content where necessary.
Give me the exact title that comes after "Article title:" of the page(s) you use for your answer. 

### Example of GPT anchored with a Mediawiki 

You can access the [GPT of the Octet.ca Wiki](https://chat.openai.com/g/g-DqRXgew1I-wiki-d-octet-ca) to run tests with the information already loaded. The original wiki can be found at [octet.ca](https://octet.ca/wiki/Accueil). 


## License

This software is released under the GNU General Public License version 3 (GPLv3). This ensures that it remains free and open source, allowing users to freely distribute, modify, and use the software under the terms of this license. For more details on the GPL v3 license, please refer to the official GNU website.

## Support and Contact

For any issues, questions, or inquiries related to this script, please feel free to reach out to us directly via email at [framagit.miguel@ptaff.ca](mailto:framagit.miguel@ptaff.ca). We are here to assist and ensure a smooth experience with our tool.

