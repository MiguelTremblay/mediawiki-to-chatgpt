#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This script is designed to extract titles and text content from MediaWiki XML dumps,
transforming the data into a text format suitable for ingestion by GPT models.
It is optimized for processing large XML files efficiently.

License: GNU General Public License v3.0
See the GNU General Public License for more details at https://www.gnu.org/licenses/gpl-3.0.html
"""

import xml.etree.ElementTree as ET
import sys

def extract_text_and_titles(xml_file_path, output_file_path):
    text_elements_found = 0

    with open(output_file_path, "w", encoding="utf-8") as output_file:
        for event, elem in ET.iterparse(xml_file_path, events=('start', 'end')):
            if event == 'start' and elem.tag == '{http://www.mediawiki.org/xml/export-0.11/}title':
                current_title = elem.text or ""
            elif event == 'start' and elem.tag == '{http://www.mediawiki.org/xml/export-0.11/}text':
                output_file.write(f"\nArticle title: {current_title}\n")
                if elem.text:
                    output_file.write(elem.text + "\n")
                text_elements_found += 1
                current_title = ""
            elem.clear()

    if text_elements_found == 0:
        print("No valid 'text' elements found in the XML file.")
    else:
        print(f"Extraction completed. {text_elements_found} 'text' elements have been written into '{output_file_path}'.")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python mediawiki-to-chatgpt-converter.py <xml_file_path> <output_file_path>")
    else:
        xml_file_path = sys.argv[1]
        output_file_path = sys.argv[2]
        extract_text_and_titles(xml_file_path, output_file_path)
