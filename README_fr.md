# Conversion d'un dump XML Mediawiki en fichier texte pour ChatGPT (GPT)

## Vue d'ensemble

Cet outil est conçu pour extraire le contenu des dumps XML de MediaWiki, le transformant en format texte adapté pour l'ingestion par les modèles GPT d'OpenAI (ChatGPT). Il est particulièrement utile pour ceux qui souhaitent utiliser leur contenu MediaWiki à des fins de formation IA, assurant une transition sans heurt des informations basées sur wiki vers un format lisible par IA.

Le script Python a été rigoureusement testé sur un dump XML de MediaWiki contenant plus de 6 000 000 lignes, garantissant sa capacité à traiter de grands ensembles de données de manière efficace et fiable.

## Prérequis 

- Python 3 installé sur votre système.
- Un fichier dump XML de votre instance MediaWiki.

## Caractéristiques

- **Traitement efficace des données** : Optimisé pour les fichiers volumineux, le script peut traiter des vidages XML de taille importante sans compromettre les performances.
- **Utilisation simple de l'interface de ligne de commande** : Facile à utiliser avec une interface de ligne de commande, nécessitant une configuration et des arguments minimaux.
- **Sortie personnalisée** : Transforme les titres et le contenu textuel de MediaWiki dans un format texte propre, ce qui le rend compatible avec GPT.

## Pour débuter

### 1. Exporter votre fichier MediaWiki

Avant d'utiliser ce script, vous devez exporter un dump XML de votre instance MediaWiki. Cela peut être fait avec un [script de maintenance PHP fourni par MediaWiki] (https://www.mediawiki.org/wiki/Manual:DumpBackup.php/fr), tel que `dumpBackup.php`. La commande ci-dessous peut être exécutée dans le répertoire d'installation de MediaWiki :

```bash 
php maintenance/dumpBackup.php --full --current > backup.xml
```

Cette commande génère une sauvegarde complète de votre wiki, en l'enregistrant sous `backup.xml`.

### 2. Transformer le dump xml de Mediawiki en texte brut

Une fois que vous avez votre dump XML, vous pouvez utiliser le script Python `mediawiki-to-chatgpt-converter` pour convertir le dump en format texte. Voici comment utiliser ce script :

    python mediawiki-to-chatgpt-converter.py <path_to_your_xml_dump> <output_file_path>

#### Arguments de la ligne de commande :

- **`<path_to_your_xml_dump>`**: Chemin d'accès au fichier de vidage XML de MediaWiki.
- **`<output_file_path>`** : Chemin où le script écrira le contenu textuel extrait.

#### Exemple:

    python mediawiki-to-chatgpt-converter.py backup.xml wiki_content.txt


## Sortie

Le script génère un fichier de sortie (`wiki_content.txt` dans l'exemple) contenant les titres et le texte de vos pages MediaWiki, formatés pour être facilement ingérés par les modèles GPT.

## Note

Ce script est optimisé pour l'efficacité, ce qui lui permet de traiter des vidages XML importants de manière fiable. Il a été testé avec des données de taille importante, ce qui garantit qu'il est adapté à différentes échelles de projet.

## Instructions pour GPT

Répondre aux questions en se basant sur le contenu du wiki fourni en entrée.

Maintenir un ton professionnel et informatif conforme au style et à l'exactitude du contenu du wiki.
Éviter de donner des opinions personnelles ou des informations non vérifiées qui ne figurent pas dans le wiki.
Utiliser les données du wiki pour enrichir les réponses en reformulant le contenu si nécessaire.
Donnez-moi le titre exact qui suit "Article title:" de la (des) page(s) que vous utilisez pour votre réponse. 

### Exemple de GPT ancré avec un Mediawiki 

Vous pouvez accéder au [GPT du Wiki de Octet.ca](https://chat.openai.com/g/g-DqRXgew1I-wiki-d-octet-ca) pour effectuer des tests avec les informations déjà chargées. Le wiki original se trouve sur [octet.ca](https://octet.ca/wiki/Accueil). 

## Licence

Ce logiciel est publié sous la licence publique générale GNU version 3 (GPLv3). Cela garantit qu'il reste libre et open source, permettant aux utilisateurs de distribuer, modifier et utiliser librement le logiciel selon les termes de cette licence. Pour plus de détails sur la licence GPL v3, veuillez vous référer au site officiel de GNU.

## Support et contact

Pour tout problème, question ou demande concernant ce script, n'hésitez pas à nous contacter directement par courrier électronique à l'adresse [framagit.miguel@ptaff.ca](mailto:framagit.miguel@ptaff.ca). Nous sommes là pour vous aider et vous garantir une expérience agréable avec notre outil.

